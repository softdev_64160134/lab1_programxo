/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab01;

/**
 *
 * @author USER
 */
public class Player {

    private String name;
    private char symbol;

    public Player(String name, char symbol) {
        setName(name);
        setSymbol(symbol);
    }

    public void setName(String name) {
        if (!name.isEmpty()) {
            this.name = name;
        }
    }
    public void setSymbol(char symbol) {
    if(symbol != '\0')
        this.symbol = symbol;
    }
    public String getName() {
        return this.name;
    }
     public char getSymbol() {
        return this.symbol;
    }
}
