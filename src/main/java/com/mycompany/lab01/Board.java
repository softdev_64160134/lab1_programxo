/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab01;

/**
 *
 * the logic for the Board class is making moves and checking for wins
 */
public class Board {

    private char board[][]; //row[1-3] and column[1-3]
    private int boardSize = 3;
    private char player1Symbol, player2Symbol;
    private int count; //number of times move in the game
    public int p1_win = 1;
    public int p2_win = 2;
    public int equal = 3;
    public int incomplete = 4;
    public int invalid = 5;

    public Board(char player1Symbol, char player2Symbol) { //create empty board
        board = new char[boardSize][boardSize];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                board[i][j] = ' ';
            }
        }
        this.player1Symbol = player1Symbol;
        this.player2Symbol = player2Symbol;

    }

    public int move(char symbol, int row, int col) {
        if (row < 0 || row >= boardSize || col < 0 || col >= boardSize || board[row][col] != ' ') {
            return invalid;
        }
        board[row][col] = symbol;
        count++;
        // Check Row
        if (board[row][0] == board[row][1] && board[row][0] == board[row][2]) {
            return symbol == player1Symbol ? p1_win : p2_win;
        }
        // Check Col
        if (board[0][col] == board[1][col] && board[0][col] == board[2][col]) {
            return symbol == player1Symbol ? p1_win : p2_win;
        }
        // First Diagonal
        if (board[0][0] != ' ' && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
            return symbol == player1Symbol ? p1_win : p2_win;
        }
        // Second Diagonal
        if (board[0][2] != ' ' && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
            return symbol == player1Symbol ? p1_win : p2_win;
        }
        if (count == boardSize * boardSize) {
            return equal;
        }
        return incomplete;
    }

    public void print() {
        System.out.println("------------------");
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                System.out.print("| " + board[i][j] + " |");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("------------------");
    }

}
